//
//  QuestionCategoryCellView.swift
//  CoreDataTest
//
//  Created by Ufuk Köşker on 20.08.2021.
//

import SwiftUI

struct QuestionCategoryCellView: View {
    @ObservedObject var questionCategoryCellViewModel = QuestionCategoryCellViewModel()
    var questionTitle: String
    var questionID: String
    var questions: [QuestionList]
    @State var selectedQuestionCD: [QuestionCD] = []
    var body: some View {
        Button(action: {
            print("title: \(questionTitle)")
            if questionCategoryCellViewModel.searchCategoryInCoreData(id: questionID) {
                print("already saved")
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    
                    print("selectedQuestion:  \(questionCategoryCellViewModel.getSelectedQuestionsFromCoreData(questionID: questionID))")
                }
            } else {
                questionCategoryCellViewModel.saveSelectedQuestionsToCoreData(questionTitle: questionTitle, id: questionID, questions: questions)
                
            }
        }) {
            Text(questionTitle)
        }
    }
}

struct QuestionCategoryCellView_Previews: PreviewProvider {
    static var previews: some View {
        QuestionCategoryCellView(questionTitle: "", questionID: "", questions: [])
    }
}
