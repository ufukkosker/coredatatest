//
//  QuestionCategories.swift
//  CoreDataTest
//
//  Created by Ufuk Köşker on 20.08.2021.
//

import SwiftUI

struct QuestionCategories: View {
    @ObservedObject var questionCategoriesViewModel: QuestionCategoriesViewModel = QuestionCategoriesViewModel()
    var body: some View {
        NavigationView {
            List {
                Section(header: Text("Already Downloaded")) {
                    ForEach(questionCategoriesViewModel.allCategories?.questionCategories ?? []) { item in
                        QuestionCategoryCellView(questionTitle: item.title, questionID: item.id, questions: item.questions)
                    }
                }
                
                Section(header: Text("Not Downloaded")) {
                    ForEach(questionCategoriesViewModel.allCategories?.questionCategories ?? []) { item in
                        QuestionCategoryCellView(questionTitle: item.title, questionID: item.id, questions: item.questions)
                            .onAppear {
                                print("aa: \(item.questions)")
                            }
                    }
                }
            }
            .navigationTitle("Questions")
        }
        .onAppear {
            questionCategoriesViewModel.getAllQuestion()
        }
    }
}

struct QuestionCategories_Previews: PreviewProvider {
    static var previews: some View {
        QuestionCategories()
    }
}
