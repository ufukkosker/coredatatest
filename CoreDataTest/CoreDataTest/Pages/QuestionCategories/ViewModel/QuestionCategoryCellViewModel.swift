//
//  QuestionCategoryCellViewModel.swift
//  CoreDataTest
//
//  Created by Ufuk Köşker on 20.08.2021.
//

import Foundation

class QuestionCategoryCellViewModel: ObservableObject {
    
    let coreDM: CoreDataManager = CoreDataManager.shared
    
    func searchCategoryInCoreData(id: String) -> Bool {
        if coreDM.searchInCategory(id: id).isEmpty {
            return false
        } else {
            return true
        }
    }
    
    func saveSelectedQuestionsToCoreData(questionTitle: String, id: String, questions: [QuestionList]) {
        coreDM.saveSelectedQuestion(questionTitle: questionTitle, id: id, questions: questions)
    }
    
    func getSelectedQuestionsFromCoreData(questionID: String) -> [QuestionCD] {
        return coreDM.getSelectedQuestion(questionID: questionID)
    }
}
