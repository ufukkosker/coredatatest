//
//  QuestionCategoriesViewModel.swift
//  CoreDataTest
//
//  Created by Ufuk Köşker on 20.08.2021.
//

import Foundation

class QuestionCategoriesViewModel: ObservableObject {
    
    @Published var allCategories: QuestionContainer?
    let localJsonService = LocalJSONService()
    func getAllQuestion() {
       allCategories = localJsonService.getQuestion(fileName: "questions", using: QuestionContainer.self)
    }
}
