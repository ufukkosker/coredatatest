//
//  CoreDataManager.swift
//  CoreDataTest
//
//  Created by Ufuk Köşker on 20.08.2021.
//

import Foundation
import CoreData

class CoreDataManager {
    static var shared = CoreDataManager()
    let persistentContainer: NSPersistentContainer
    
    init() {
        
        persistentContainer = NSPersistentContainer(name: "QuestionsCoreData")
        QuestionsValueTransformer.register()
        persistentContainer.loadPersistentStores { description, error in
            if let error = error {
                fatalError("Core Data Stre failed: \(error.localizedDescription)")
            }
        }
    }
    
    func searchInCategory(id: String) -> [QuestionCD] {
        let fetchRequest: NSFetchRequest<QuestionCD> = QuestionCD.fetchRequest()
        let search = NSPredicate(format: "id == %@", id)
        print("search: \(search)")
        fetchRequest.predicate = search
        
        do {
            return try persistentContainer.viewContext.fetch(fetchRequest)
        } catch {
            print("no Data \n")
            return []
        }
    }
    
    func saveSelectedQuestion(questionTitle: String, id: String, questions: [QuestionList]) {
        
        let questionsCD = QuestionCD(context: persistentContainer.viewContext)
        questionsCD.title = questionTitle
        questionsCD.id = id
        questionsCD.questions?.questionList = questions
        print("nil test: \(questionsCD.questions?.questionList ?? [])")
        
        do {
            try persistentContainer.viewContext.save()
        } catch let error {
            print("Failed to save selected category: \(error.localizedDescription)")
        }
    }
    
    func getSelectedQuestion(questionID: String) -> [QuestionCD] {
        let fetchRequest: NSFetchRequest<QuestionCD> = QuestionCD.fetchRequest()
        let search = NSPredicate(format: "id == %@", questionID)
        print("search: \(search)")
        fetchRequest.predicate = search
        fetchRequest.returnsObjectsAsFaults = false
        print("request predicate: \(String(describing: fetchRequest.predicate))")
        do {
            return try persistentContainer.viewContext.fetch(fetchRequest)
        } catch let error {
            print("get hata: \(error.localizedDescription)")
            return []
        }
    }
}
