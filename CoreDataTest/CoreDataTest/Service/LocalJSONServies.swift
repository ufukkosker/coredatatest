//
//  LocalJSONServies.swift
//  CoreDataTest
//
//  Created by Ufuk Köşker on 20.08.2021.
//

import Foundation

final class LocalJSONService: ObservableObject {
    func getQuestion<T: Codable>(fileName: String, using modelType: T.Type)  -> T? {
        if let url = Bundle.main.url(forResource: fileName, withExtension: "json") {
            do {
                let data = try Data(contentsOf: url)
                let decoder = JSONDecoder()
                let jsonData = try decoder.decode(modelType, from: data)
                return jsonData
            } catch {
                print("error:\(error)")
            }
        }
        return nil
    }
}
