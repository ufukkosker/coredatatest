//
//  QuestionsNSSecureCoding.swift
//  CoreDataTest
//
//  Created by Ufuk Köşker on 21.08.2021.
//

import Foundation

public class QuestionsNSSecureCoding: NSObject, NSSecureCoding {
    
    public static var supportsSecureCoding: Bool = true
    
    var questionList: [QuestionList]

    
    required init(questions: [QuestionList]) {
        self.questionList = questions

    }
    
    public func encode(with coder: NSCoder) {
        coder.encode(questionList, forKey: "questionList")

    }
    
    required public init?(coder: NSCoder) {
        
        questionList = coder.decodeObject(of: NSArray.self, forKey: "questionList") as? Array<QuestionList> ?? []

    }
}

@objc(QuestionsValueTransformer)
final class QuestionsValueTransformer: NSSecureUnarchiveFromDataTransformer {
    static let name = NSValueTransformerName(rawValue: String(describing: QuestionsValueTransformer.self))
 
    override static var allowedTopLevelClasses: [AnyClass] {
        return [QuestionList.self]
    }
    
    public static func register() {
            let transformer = QuestionsValueTransformer()
            ValueTransformer.setValueTransformer(transformer, forName: name)
        }
}
