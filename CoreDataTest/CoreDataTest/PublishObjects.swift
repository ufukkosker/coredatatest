//
//  PublishObjects.swift
//  CoreDataTest
//
//  Created by Ufuk Köşker on 20.08.2021.
//

import Foundation

class PublishObjects: ObservableObject {
    @Published var allQuestions: QuestionCD = QuestionCD()
}
