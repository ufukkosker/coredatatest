//
//  Question.swift
//  CoreDataTest
//
//  Created by Ufuk Köşker on 20.08.2021.
//

import Foundation

class QuestionContainer: Codable {
    
    var questionCategories: [Question]
    
    init(questionCategories: [Question]) {
        self.questionCategories = questionCategories
    }
}

class Question: Codable, Identifiable {
    
    var title: String
    var id: String
    var questions: [QuestionList]
    
        init(title: String, id: String, questions: [QuestionList]) {
            self.title = title
            self.id = id
            self.questions = questions
        }
}

class QuestionList: Codable, Identifiable {
    
    
    var id: String
    //    var question: String
    //    var isQuestionImage, isSectionImage: Bool
    //    var imageURL: String
    //    var imageData: Data?
    //    var sections: [QuestionSections.RawValue : String]
    //    var selected: String
    //    var correct: String
    
    init(id: String) {
        self.id = id
        //        //        self.question = question
        //        //        self.isQuestionImage = isQuestionImage
        //        //        self.isSectionImage = isSectionImage
        //        //        self.imageURL = imageURL
        //        //        self.imageData = imageData
        //        //        self.sections = sections
        //        //        self.selected = selected
        //        //        self.correct = correct
    }
    
    
    //    public func encode(with coder: NSCoder) {
    //
    //    }
    
    
    
}

enum QuestionSections: String, Codable {
    case A = "A"
    case B = "B"
    case C = "C"
    case D = "D"
}


//public protocol NSCoding {
//    func encode(with coder: NSCoder)
//    init?(coder: NSCoder) // NS_DESIGNATED_INITIALIZER
//}
